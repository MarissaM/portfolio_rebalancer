This is a portfolo rebalancing program that will take a csv file with the information about your portfolio and target allocation and return to you the number of shares you should buy and sell in order to attain your target portfolio.

## How to Use

The csv file should have headers - an example can be found in portfolio.csv

Run:
```
  $ ruby run.rb location_of_csv_file
```

## Technical Choices

* I used ruby because it is a language I am familiar with
* I did not want to use rails because it is a simple application without a UI
  but I included ActiveModel::Validations for validations

## What could be better

* Include transaction costs in the calculations
* Fix the output when you buy/sell a single share
* Include an allocation for cash
* It could accept other inputs(inputs other than csv files or csv files without headers etc.)