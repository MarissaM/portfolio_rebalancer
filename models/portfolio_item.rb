require 'active_model'
class PortfolioItem
  include ActiveModel::Validations
  attr_accessor :portfolio, :price, :owned, :target, :ticker, :additional_to_purchase
  validates_presence_of :ticker
  validates_numericality_of :price, greater_than: 0
  validates_numericality_of :target, greater_than_or_equal_to: 0, less_than_or_equal_to: 100

  def initialize(attributes = {})
    @price = attributes["price"].to_f
    @owned = attributes["owned"].to_i
    @target = attributes["target"].to_f
    @ticker = attributes["ticker"]
    @additional_to_purchase = 0
  end

  def value
    price * owned
  end

  def target_investment
    target/100.00 * portfolio.value
  end

  def target_shares
    (target_investment / price).floor
  end

  def proposed_investment
    (target_shares + additional_to_purchase) * price
  end

  def share_difference
    target_shares + additional_to_purchase - owned
  end
end

