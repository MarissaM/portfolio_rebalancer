require 'active_model'
class Portfolio
  include ActiveModel::Validations
  attr_accessor :portfolio_items
  validate :targets_add_to_100

  def initialize(portfolio_items = [])
    @portfolio_items = portfolio_items
    @portfolio_items.each do |item|
      item.portfolio = self
    end
  end

  def value
    sum = 0
    portfolio_items.each{|item| sum += item.value}
    sum
  end

  def proposed_total_investment
    sum = 0
    portfolio_items.each{|item| sum = sum + item.proposed_investment}
    sum
  end

  def additional_investment_needed
    value - proposed_total_investment
  end

  def set_additional_shares_needed
    sorted_items = portfolio_items.sort{|a,b| b.price <=> a.price}
    until additional_investment_needed < sorted_items.last.price do
      portfolio_items = sorted_items.select{|item| item.price <= additional_investment_needed}
      portfolio_items.each do |item|
        item.additional_to_purchase += 1 if item.price <= additional_investment_needed
      end
    end
  end

  def rebalance
    set_additional_shares_needed
    portfolio_items.each do |item|
      if item.share_difference > 0
        puts "Buy #{item.share_difference.abs} shares of #{item.ticker}"
      else
        puts "Sell #{item.share_difference.abs} shares of #{item.ticker}"
      end
    end
  end

  private 

  def targets_add_to_100
    target_sum = portfolio_items.inject(0) { |sum, p| sum + p.target }
    errors.add(:base, 'Target percentages must add to 100%') unless target_sum.to_i == 100
  end

end

