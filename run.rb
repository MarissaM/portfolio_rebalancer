require 'csv'
require_relative 'models/portfolio_item'
require_relative 'models/portfolio'
file = ARGV.first
portfolio_items = []
CSV.foreach(file, headers: true) do |row|
  portfolio_items << PortfolioItem.new(row.to_hash)
end
if portfolio_items.all?(&:valid?)
  portfolio = Portfolio.new(portfolio_items)
  if portfolio.valid?
    portfolio.rebalance
  else
    puts "ERROR: #{portfolio.errors.messages}"
  end
else
  portfolio_items.each do |item|
    unless item.valid?
      puts "ERROR: #{item.errors.messages}"
    end
  end
end
