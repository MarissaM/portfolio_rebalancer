require 'rspec'
require_relative '../models/portfolio_item'

describe PortfolioItem do
  let(:portfolio_item){PortfolioItem.new({"ticker" => 'GOOG', "target" => "60", "owned" => "52", "price" => "98"})}
  let(:portfolio_item_2){PortfolioItem.new({"ticker" => 'APPL', "target" => "30", "owned" => "136", "price" => "22"})}
  let(:portfolio_item_3){PortfolioItem.new({"ticker" => 'TSLA', "target" => "10", "owned" => "239", "price" => "8"})}

  describe "should validate that the csv has all the required information" do
    it 'should require a ticker' do
      portfolio_item.ticker = ''
      expect(portfolio_item).to_not be_valid
      expect(portfolio_item.errors.messages[:ticker]).to include("can't be blank")
    end

    it 'should require a stock price with a value greater than 0' do
      portfolio_item.price = 0
      expect(portfolio_item).to_not be_valid
      expect(portfolio_item.errors.messages[:price]).to include("must be greater than 0")
    end
    it 'should require a target with a percentage greater than or equal to 0' do
      portfolio_item.target = -3
      expect(portfolio_item).to_not be_valid
      expect(portfolio_item.errors.messages[:target]).to include("must be greater than or equal to 0")
      portfolio_item.target = 105
      expect(portfolio_item).to_not be_valid
      expect(portfolio_item.errors.messages[:target]).to include("must be less than or equal to 100")
    end
  end

  context "if you are selling shares" do
    it "should have more shares than the shares that are being sold" do
      portfolio = Portfolio.new([portfolio_item, portfolio_item_2, portfolio_item_3])
      change_in_shares = portfolio_item_2.owned + portfolio_item_2.share_difference
      expect(change_in_shares).to be >= 0
    end
  end
end
