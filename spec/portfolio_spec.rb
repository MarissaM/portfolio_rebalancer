require 'rspec'
require_relative '../models/portfolio_item'
require_relative '../models/portfolio'

describe Portfolio do
  let(:portfolio_item_1){PortfolioItem.new({"ticker" => 'GOOG', "target" => "60", "owned" => "52", "price" => "98"})}
  let(:portfolio_item_2){PortfolioItem.new({"ticker" => 'APPL', "target" => "30", "owned" => "136", "price" => "22"})}
  let(:portfolio_item_3){PortfolioItem.new({"ticker" => 'TSLA', "target" => "10", "owned" => "239", "price" => "8"})}
  let(:portfolio) {Portfolio.new([portfolio_item_1, portfolio_item_2, portfolio_item_3])}

  it 'should have allocation target percentages that add to 100' do
    portfolio_item_1.target = 80
    expect(portfolio).to_not be_valid
    expect(portfolio.errors.messages[:base]).to include("Target percentages must add to 100%")
  end

  describe '#rebalance' do
    before(:each) do
      portfolio.rebalance
    end
    describe "unused portfolio value" do
      let(:unused_portfolio_value){portfolio.value - portfolio.proposed_total_investment}
      let(:lowest_share_price){portfolio.portfolio_items.map(&:price).min}

      it 'should have used as much of the investment as possible' do
        expect(unused_portfolio_value).to be < lowest_share_price
      end

      it "should not have used more than the current portfolio value" do
        expect(unused_portfolio_value).to be >= 0
      end
    end
    
    it "should not have greater total buys than total sells" do
      sum = 0
      portfolio.portfolio_items.each do |item|
        sum += item.share_difference * item.price
      end
      expect(sum).to be >= 0
    end
  end
end
